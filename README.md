# README #

Bootstap 4.0 and UI development

### What is this repository for? ###

* Quick summary
* 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

##
Bootstrap 4 es una de las herramientas más populares y prestigiosas del mundo, ampliamente utilizada en los proyectos tecnológicos por su belleza estética, simplicidad de uso, amplia variedad de componentes y de fácil adaptación a las necesidades de los proyectos.


Por otra parte, la comunidad de usuarios de Bootstrap es enorme y de todas partes del mundo. Cualquier inquietud o duda que tengas, siempre habrá alguien bien dispuesto a ayudarte desde algún punto del planeta. En los siguientes módulos aprenderás a usar el sistema de grillas, que permite adaptar el contenido de tu web de una forma sencilla y elegante, en un lenguaje claro y comprensible para cualquier desarrollador.

Veremos las diferentes opciones que tenemos para hacer un diseño responsive y que, a su vez, nos sirva para una web vista desde una pantalla de escritorio.

El objetivo es tener un conocimiento general de la herramienta y una intuición sobre cómo avanzar en los diferentes puntos de tu proyecto. Nos concentraremos en los componentes CCS de Bootstrap, botones, tags, pills, tablas, tarjetas, imágenes, alertas y barras de progresos.

Crearemos formularios que nos servirán para interactuar con los usuarios y pedirles que ingresen información o permitirles realizar acciones. Utilizaremos las barras de navegación y diferentes conceptos de navegabilidad para lograr transmitir el mapa de nuestro sitio y aplicación.

Un uso claro e intuitivo de la navegación contribuye a una mejor experiencia del usuario final. En el módulo 3, trabajaremos con los componentes JavaScript de Bootstrap. A diferencia de los anteriores, estos componentes incluyen comportamiento al sitio utilizando JavaScript.

Estos componentes permiten configuraciones y usos variados utilizando tu propio JavaScript. Utilizaremos botones, navegación con botones, Affix, pestañas, diferentes tipos de alerta y ventanas emergentes, Scrollspy, Toolstip y carruseles.

El final se estará en condiciones de crear con gran detalle tu propio proyecto. Pondremos nuestro sitio en producción, automatizaremos tareas de compilación de CCS, de JavaScript y todo el contenido de HTML que hayamos desarrollado. Haremos que todo el mundo vea nuestro proyecto. También avanzaremos con el comportamiento de nuestra web, utilizando jQuery.
